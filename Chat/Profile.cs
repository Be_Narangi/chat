﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Chat
{
    public class Profile
    {
        public string Nickname   { get; set; }

        public string Id         { get; set; }

        public string AvatarPath { get; set; }

        public string Status     { get; set; }

        public IPAddress Address { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }        // TODO other "About" | tuple?
    }
}
