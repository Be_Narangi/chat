﻿//      *********    НЕ ИЗМЕНЯЙТЕ ЭТОТ ФАЙЛ     *********
//      Этот файл обновляется средством разработки. Внесение
//      изменений в этот файл может привести к ошибкам.
namespace Expression.Blend.SampleData.SampleDataSource
{
    using System; 
    using System.ComponentModel;

// Чтобы значительно уменьшить объем примеров данных в рабочем приложении, можно задать
// константу условной компиляции DISABLE_SAMPLE_DATA и отключить пример данных во время выполнения.
#if DISABLE_SAMPLE_DATA
    internal class SampleDataSource { }
#else

    public class SampleDataSource : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public SampleDataSource()
        {
            try
            {
                Uri resourceUri = new Uri("/Chat;component/SampleData/SampleDataSource/SampleDataSource.xaml", UriKind.RelativeOrAbsolute);
                System.Windows.Application.LoadComponent(this, resourceUri);
            }
            catch
            {
            }
        }

        private Contacts _Contacts = new Contacts();

        public Contacts Contacts
        {
            get
            {
                return this._Contacts;
            }
        }

        private Statuses _Statuses = new Statuses();

        public Statuses Statuses
        {
            get
            {
                return this._Statuses;
            }
        }

        private System.Windows.Media.ImageSource _Avatar = null;

        public System.Windows.Media.ImageSource Avatar
        {
            get
            {
                return this._Avatar;
            }

            set
            {
                if (this._Avatar != value)
                {
                    this._Avatar = value;
                    this.OnPropertyChanged("Avatar");
                }
            }
        }
    }

    public class Contacts : System.Collections.ObjectModel.ObservableCollection<ContactsItem>
    { 
    }

    public class ContactsItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _Nickname = string.Empty;

        public string Nickname
        {
            get
            {
                return this._Nickname;
            }

            set
            {
                if (this._Nickname != value)
                {
                    this._Nickname = value;
                    this.OnPropertyChanged("Nickname");
                }
            }
        }

        private bool _Status = false;

        public bool Status
        {
            get
            {
                return this._Status;
            }

            set
            {
                if (this._Status != value)
                {
                    this._Status = value;
                    this.OnPropertyChanged("Status");
                }
            }
        }

        private System.Windows.Media.ImageSource _Avatar = null;

        public System.Windows.Media.ImageSource Avatar
        {
            get
            {
                return this._Avatar;
            }

            set
            {
                if (this._Avatar != value)
                {
                    this._Avatar = value;
                    this.OnPropertyChanged("Avatar");
                }
            }
        }
    }

    public class Statuses : System.Collections.ObjectModel.ObservableCollection<StatusesItem>
    { 
    }

    public class StatusesItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _Text = string.Empty;

        public string Text
        {
            get
            {
                return this._Text;
            }

            set
            {
                if (this._Text != value)
                {
                    this._Text = value;
                    this.OnPropertyChanged("Text");
                }
            }
        }

        private System.Windows.Media.ImageSource _Icon = null;

        public System.Windows.Media.ImageSource Icon
        {
            get
            {
                return this._Icon;
            }

            set
            {
                if (this._Icon != value)
                {
                    this._Icon = value;
                    this.OnPropertyChanged("Icon");
                }
            }
        }
    }
#endif
}
