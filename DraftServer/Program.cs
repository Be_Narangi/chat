﻿using System;

namespace DraftServer
{
    class Program
    {        
        static void Main(string[] args)
        {
            var server = new ChatServer();
            server.Start();
        }
    }
}
