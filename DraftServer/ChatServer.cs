﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace DraftServer
{
    public class ChatServer
    {
        IPAddress addr = new IPAddress(new byte[] { 127, 0, 0, 1 });
        int port = 55555;
        
        public bool IsStopped { get; private set; }

        public void Start()
        {
            var listener = new TcpListener(addr, port);
            listener.Start();
            var socket = listener.AcceptSocket();
            var serverStream = new NetworkStream(socket);

            using (var sr = new StreamReader(serverStream, Encoding.UTF8))
            using (var sw = new StreamWriter(serverStream, Encoding.UTF8))
            {
                while (true)
                {
                    var inboxMessageBuilder = new StringBuilder();


                    var msg = sr.ReadLine();

                    sw.WriteLine(msg);

                    if (IsStopped) break;
                }
            }

            listener.Stop();
        }
    }



}
