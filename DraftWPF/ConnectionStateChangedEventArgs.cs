﻿using System;

namespace DraftWPF
{
    public enum Statuses
    {
        Connected,
        Disconnected,
        ConnectionFailed
    }

    public class ConnectionStateChangedEventArgs : EventArgs
    {
        public Statuses Status { get; set; }

        public ConnectionStateChangedEventArgs(Statuses s)
        {
            Status = s;
        }
    }
}