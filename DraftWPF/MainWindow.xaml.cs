﻿using System;
using System.Windows;
using System.Windows.Input;

namespace DraftWPF
{
    public partial class MainWindow : Window
    {
        public ChatViewModel ChatVM { get; }

        public MainWindow()
        {
            ChatVM = new ChatViewModel();
            InitializeComponent();
        }

        private void SendMessage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChatVM.SendMessage();
            ChatVM.OutgoingMessage = String.Empty;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ChatVM.Connect();
        }
    }
}
