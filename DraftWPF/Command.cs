﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DraftWPF
{
    public static class Command
    {
        public static readonly RoutedUICommand SendMessage = new RoutedUICommand("Send message", 
                                                                                 "SendMessage", 
                                                                                 typeof(MainWindow));
    }
}
