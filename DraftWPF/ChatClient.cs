﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace DraftWPF
{
    public class ChatClient
    {
        public bool IsConnected { get; set; }

        readonly IPAddress addr = new IPAddress(new byte[] { 127, 0, 0, 1 });
        const int port = 55555;
        IPEndPoint m_EndPoint;
        TcpClient m_Client;
        NetworkStream m_ClientStream;

        public async void Connect()
        {
            m_EndPoint = new IPEndPoint(addr, port);
            m_Client = new TcpClient();

            try
            {
                m_Client.Connect(m_EndPoint);
            }
            catch (SocketException)
            {
                RaiseConnectionStateChanged(Statuses.ConnectionFailed);
                return;
            }

            IsConnected = true;
            m_ClientStream = new NetworkStream(m_Client.Client);
            
            RaiseConnectionStateChanged(Statuses.Connected);

            using (var sr = new StreamReader(m_ClientStream, Encoding.UTF8))
            {
                while (true)
                {
                    var inboxMessageBuilder = new StringBuilder();

                    var receivedMessage = await sr.ReadLineAsync();
                    RaiseMessageReceived(receivedMessage);                    
                }
            }
        }        

        public void Send(string outgoingMessage)
        {
            using (var sw = new StreamWriter(m_ClientStream, Encoding.UTF8))
            {
                sw.WriteLine(outgoingMessage);
                sw.Flush();
            }
        }

        public event EventHandler<ConnectionStateChangedEventArgs> ConnectionStateChanged;
        private void RaiseConnectionStateChanged(Statuses s)
        {
            ConnectionStateChanged?.Invoke(this, new ConnectionStateChangedEventArgs(s));
        }

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        private void RaiseMessageReceived(string receivedMessage)
        {
            MessageReceived?.Invoke(this, new MessageReceivedEventArgs(receivedMessage));
        }
    }
}