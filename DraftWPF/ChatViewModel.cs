﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DraftWPF
{
    public class ChatViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<ChatMessage> ChatHistory { get; }

        string m_OutgoingMessage;
        public string OutgoingMessage
        {
            get { return m_OutgoingMessage; }
            set
            {
                m_OutgoingMessage = value;
                notifyPropertyChanged();
            }
        }

        string m_StatusIcon;
        public string StatusIcon
        {
            get => m_StatusIcon;
            set
            {
                m_StatusIcon = value;
                notifyPropertyChanged();
            }
        }

        public void Connect()
        {
            m_Client.Connect();
        }

        ChatClient m_Client;

        public ChatViewModel()
        {
            m_Client = new ChatClient();
            m_Client.ConnectionStateChanged += Client_ConnectionStateChanged;
            m_Client.MessageReceived += Client_MessageReceived;

            ChatHistory = new ObservableCollection<ChatMessage>()
            {
                new ChatMessage("Hello!")
                {
                    Sender = "Me",
                    Time = DateTime.Now
                },

                new ChatMessage("Hi!")
                {
                    Sender = "You",
                    Time = DateTime.Now
                }
            };
        }

        private void Client_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            ChatHistory.Add(new ChatMessage(e.ReceivedMessage));
        }

        private void Client_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            switch (e.Status)
            {
                case Statuses.Connected:
                    StatusIcon = "/Images/online-icon.png";
                    break;
                case Statuses.Disconnected:
                    StatusIcon = "/Images/offline-icon.png";
                    break;
                case Statuses.ConnectionFailed:
                    StatusIcon = "/Images/offline-icon.png";
                    break;
                default:
                    StatusIcon = "/Images/neutral-icon.png";
                    break;
            }
        }

        private void StartChat()
        {
            throw new NotImplementedException();
        }

        public void SendMessage()
        {
            m_Client.Send(OutgoingMessage);
            ChatHistory.Add(new ChatMessage(OutgoingMessage));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void notifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}