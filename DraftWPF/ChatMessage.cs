﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace DraftWPF
{
    public class ChatMessage
    {
        public string    Sender  { get; set; }
        
        public DateTime  Time    { get; set; }

        public string    Content { get; set; }

        public ChatMessage(string content)
        {
            Content = content;
        }        
    }
}