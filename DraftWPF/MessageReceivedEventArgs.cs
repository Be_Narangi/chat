﻿namespace DraftWPF
{
    public class MessageReceivedEventArgs
    {
        public string ReceivedMessage { get; }

        public MessageReceivedEventArgs(string receivedMessage)
        {
            ReceivedMessage = receivedMessage;
        }
    }
}