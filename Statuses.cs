﻿using System;

public enum Statuses
{
	Online,
    Offline,
    Busy,
    AFK,
    Invisible
}
